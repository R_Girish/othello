package othello;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class OthelloGUI extends JFrame {

	private static final long serialVersionUID = -3027179269173623528L;

	private final Color BEIGE = new Color(232, 223, 205);
	private final int BOARD_X = 570; 	// x-pos of board GUI
	private final int BOARD_Y = 120; 	// y-pos of board GUI
	private final int BOARD_CELLS = 8;  // # cells in x and y direction
	private final int CELL_SIZE = 70;	// Cell size in pixels (always square)
	private final int IMAGE_MARGIN = 4; // margin around tile images within a cell

	private GridView othelloView;
	private JLabel player1Label;
	private JLabel player2Label;
	private JLabel player1NumPieces;	
	private JLabel player2NumPieces;	
	private JTextArea gameResultsArea;
	private JTextArea tournamentStandingsArea;
	private ScrollPanel messagePanel;
	private JCheckBox continuousModeCheckBox;
	private JCheckBox pauseAfterGameCheckBox;
	private JCheckBox pauseOnWarningCheckBox;
	private JComboBox<String> speedComboBox;
	private JButton playButton;
	private JMenuItem humanMatchMenuItem;
	private JMenuItem tournmentMenuItem;
	private JMenuItem exitMenuItem;
	private JMenuItem resetStatsMenuItem;
	private JMenuItem tournamentRoundRobinsMenuItem;
	
	public OthelloGUI() {

		// Set up overall GUI Frame
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setSize(1200, 780);
		this.setTitle("Othello");
		this.setLayout(null);
		
		// Set up menu
		JMenuBar menubar = new JMenuBar(); 
		JMenu gameMenu = new JMenu("Game");
		humanMatchMenuItem = new JMenuItem("Player vs Player Game", 'p');
		tournmentMenuItem = new JMenuItem("AI Tournament", 't');
		exitMenuItem = new JMenuItem("Exit", 'x');
		JMenu optionsMenu = new JMenu("Options");
		resetStatsMenuItem = new JMenuItem("Reset Stats");
		tournamentRoundRobinsMenuItem = new JMenuItem("Set Tournament Round Robins");
		gameMenu.add(humanMatchMenuItem);
		gameMenu.add(tournmentMenuItem);
		gameMenu.add(exitMenuItem);
		optionsMenu.add(resetStatsMenuItem);
		optionsMenu.add(tournamentRoundRobinsMenuItem);
		menubar.add(gameMenu);
		menubar.add(optionsMenu);
		this.setJMenuBar(menubar);
		
		// The raw data for Othello is stored here
		Board board = new Board(BOARD_CELLS, BOARD_CELLS);

		// Number of pieces are displayed in these JLabels
		player1NumPieces = new JLabel("2");
		player1NumPieces.setFont(new Font("Arial", Font.BOLD, 48));;
		player2NumPieces = new JLabel("2");
		player2NumPieces.setFont(new Font("Arial", Font.BOLD, 48));;

		// This is the GUI of the Othello board
		othelloView = new GridView(BOARD_CELLS, BOARD_CELLS, CELL_SIZE, IMAGE_MARGIN,  1, board);
		othelloView.setLayout(null);
		othelloView.setBounds(0, 0, (int) othelloView.getPreferredSize().getWidth(), (int) othelloView.getPreferredSize().getHeight());
		
		try {
			othelloView.setBackground(ImageIO.read(new File("images" + System.getProperty("file.separator") + "parchment.jpg")));
		} catch (IOException e) {
			e.printStackTrace();
		}

		othelloView.setBounds(BOARD_X, BOARD_Y, (int) othelloView.getPreferredSize().getWidth(),
				(int) othelloView.getPreferredSize().getHeight());
		
		// Message area, seen as a scroll on the GUI
		messagePanel = new ScrollPanel();
		messagePanel.setOpaque(false);
		messagePanel.setBounds(BOARD_X, BOARD_Y - 100, (int) othelloView.getPreferredSize().getWidth(), 100);

		// Overlay where intended moves and winning pieces are highlighted
		HighlightPanel highlightPanel = new HighlightPanel(CELL_SIZE);
		highlightPanel.setLayout(null);
		highlightPanel.setBounds(0, 0, (int) othelloView.getPreferredSize().getWidth(),
								(int) othelloView.getPreferredSize().getHeight());
		this.setGlassPane(highlightPanel);
		highlightPanel.setVisible(true);

		// Player 1's game info is displayed here
		JPanel player1Panel = new JPanel();
		player1Panel.setOpaque(false);
		player1Panel.setPreferredSize(new Dimension(250, 150));
		TitledBorder player1Border = new TitledBorder(new LineBorder(Color.BLACK, 3, true), "Player 1");
		player1Border.setTitleFont(new Font("Arial", Font.BOLD, 16));
		player1Panel.setBorder(player1Border);
		Image img = new ImageIcon("images" + System.getProperty("file.separator") + "white.png").getImage();
		BufferedImage bi = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		g.drawImage(img, 0, 0, 32, 32, null);
		g.dispose();
		ImageIcon player1Icon = new ImageIcon(bi);
		player1Label = new JLabel("Player 1", player1Icon, SwingConstants.CENTER);
		player1Panel.add(player1Label);
		JPanel player1CaptureFrame = new JPanel();
		player1CaptureFrame.setOpaque(false);
		player1CaptureFrame.setPreferredSize(player1Panel.getPreferredSize());
		player1CaptureFrame.add(player1NumPieces);
		player1Panel.add(player1CaptureFrame);

		// Player 2's game info is displayed here
		JPanel player2Panel = new JPanel();
		player2Panel.setOpaque(false);
		player2Panel.setPreferredSize(new Dimension(250, 150));
		TitledBorder player2Border = new TitledBorder(new LineBorder(Color.BLACK, 3, true), "Player 2");
		player2Border.setTitleFont(new Font("Arial", Font.BOLD, 16));
		player2Panel.setBorder(player2Border);
		Image img2 = new ImageIcon("images" + System.getProperty("file.separator") + "black.png").getImage();
		BufferedImage bi2 = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics g2 = bi2.createGraphics();
		g2.drawImage(img2, 0, 0, 32, 32, null);
		g2.dispose();
		ImageIcon player2Icon = new ImageIcon(bi2);
		player2Label = new JLabel("Player 2", player2Icon, SwingConstants.CENTER);
		player2Panel.add(player2Label);
		JPanel player2CaptureFrame = new JPanel();
		player2CaptureFrame.setOpaque(false);
		player2CaptureFrame.setPreferredSize(player2Panel.getPreferredSize());
		player2CaptureFrame.add(player2NumPieces);
		player2Panel.add(player2CaptureFrame);

		// Game Results
		gameResultsArea = new JTextArea(8, 65);
		gameResultsArea.setEditable(false);
		gameResultsArea.setBackground(BEIGE);
		gameResultsArea.setFont(new Font("Courier New", Font.PLAIN, 12));
		JScrollPane gameResultsPane = new JScrollPane(gameResultsArea);
		gameResultsPane.setOpaque(false);
		TitledBorder gameResultsBorder = new TitledBorder(new LineBorder(Color.BLACK, 3, true), "Game Results");
		gameResultsBorder.setTitleFont(new Font("Arial", Font.BOLD, 16));
		gameResultsPane.setBorder(gameResultsBorder);

		// Tournament standings
		tournamentStandingsArea = new JTextArea(8, 60);
		tournamentStandingsArea.setEditable(false);
		tournamentStandingsArea.setBackground(BEIGE);
		tournamentStandingsArea.setFont(new Font("Courier New", Font.PLAIN, 12));
		JScrollPane tournamentStandingsPane = new JScrollPane(tournamentStandingsArea);
		tournamentStandingsPane.setOpaque(false);
		TitledBorder tournamentStandingsBorder = new TitledBorder(new LineBorder(Color.BLACK, 3, true), "Tournament Standings");
		tournamentStandingsBorder.setTitleFont(new Font("Arial", Font.BOLD, 16));
		tournamentStandingsPane.setBorder(tournamentStandingsBorder);

		// Panel containing the game and tournament results
		JPanel resultsPanel = new JPanel();
		resultsPanel.setBackground(Color.pink);
		resultsPanel.setOpaque(false);
		resultsPanel.setPreferredSize(new Dimension(500, 300));
		resultsPanel.add(gameResultsPane);
		resultsPanel.add(tournamentStandingsPane);

		// Set up left half of GUI w/ game info
		JPanel leftPanel = new JPanel();
		leftPanel.setOpaque(false);
		JLabel title = new JLabel(new ImageIcon("images" + System.getProperty("file.separator") + "pente_title.png"));
		leftPanel.add(title);
		leftPanel.setBounds(20, 20, 550, 690);
		JPanel leftSubPanel = new JPanel();
		leftSubPanel.setOpaque(false);
		leftSubPanel.add(player1Panel);
		leftSubPanel.add(player2Panel);
		leftPanel.add(leftSubPanel);		
		leftPanel.add(resultsPanel);

		// Game controls JPanel
		JPanel controlPanel = new JPanel();
		controlPanel.setBackground(BEIGE);
		controlPanel.setBorder(new LineBorder(Color.black, 3, true));
		controlPanel.setPreferredSize(new Dimension(500, 110));
		JPanel controlPanelSub = new JPanel();
		controlPanelSub.setLayout(new GridLayout(1, 2));
		controlPanelSub.setPreferredSize(new Dimension(480, 60));
		controlPanelSub.setOpaque(false);
		controlPanelSub.setBackground(Color.YELLOW);
		JPanel controlPanelLeft = new JPanel();
		controlPanelLeft.setOpaque(false);
		continuousModeCheckBox = new JCheckBox("Continuous Mode", false);
		controlPanelLeft.add(continuousModeCheckBox);
		pauseAfterGameCheckBox = new JCheckBox("Pause After Game", true);
		controlPanelLeft.add(pauseAfterGameCheckBox);
		JPanel controlPanelRight = new JPanel();
		controlPanelRight.setOpaque(false);
		String[] speedItems = {"No Delay", "Fast", "Medium", "Slow"};
		speedComboBox = new JComboBox<String>(speedItems);
		speedComboBox.setActionCommand("Change Speed");
		pauseOnWarningCheckBox = new JCheckBox("Pause on Warnings", false);
		controlPanelRight.add(pauseOnWarningCheckBox);
		controlPanelRight.add(Box.createRigidArea(new Dimension(30, 10)));
		controlPanelRight.add(speedComboBox);
		controlPanelSub.add(controlPanelLeft);
		controlPanelSub.add(controlPanelRight);		
		controlPanel.add(controlPanelSub);
		playButton = new JButton("Step");
		playButton.setActionCommand("Play");
		controlPanel.add(playButton);
		leftPanel.add(controlPanel);
		
		// Add top level GUI panels
		this.add(leftPanel);
		this.add(othelloView);
		this.add(messagePanel);

		// Add background Image
		try {
			final BufferedImage backgroundImage;
			backgroundImage = ImageIO.read(new File("images" + System.getProperty("file.separator") + "pente_background.png"));
			class BackgroundPanel extends JPanel {
				private static final long serialVersionUID = -6715864334078002215L;
				@Override
				protected void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.drawImage(backgroundImage, 0, 0, getParent().getWidth(), getParent().getHeight(), null);
				}				
			}
			// Background
			BackgroundPanel bg = new BackgroundPanel();
			bg.setBounds(0, 0, this.getWidth(), this.getHeight());
			this.add(bg);
		} catch (IOException e) {
			e.printStackTrace();
		}		

		// Create a controller that updates this GUI
		new OthelloController(this, othelloView, board, highlightPanel);
		othelloView.grabFocus();
		this.revalidate();
	}
	
	public void addGameInfo(String line) {
		gameResultsArea.append(line + "\n");
	}

	public void clearGameInfo() {
		gameResultsArea.setText("");
	}

	public void setTournamentInfo(String line) {
		tournamentStandingsArea.setText(line);
	}
	
	public void setVSMessage(String p1Name, String p2Name, Image p1Image, Image p2Image) {
		messagePanel.setVSMessage(p1Name, p2Name, p1Image, p2Image);
	}
	
	/**
	 * Updates the label for Player 1's name
	 * @param name
	 */
	public void setPlayer1Name(String name) {
		player1Label.setText(name);
		repaint();
	}

	/**
	 * Updates the label for Player 2's name
	 * @param name
	 */
	public void setPlayer2Name(String name) {
		player2Label.setText(name);
		repaint();
	}

	/**
	 * Updates the number of pieces for Player 1
	 * @param name
	 */
	public void setPlayer1NumPieces(String numPieces) {
		player1NumPieces.setText(numPieces);
		repaint();
	}

	/**
	 * Updates the number of pieces for Player 2
	 * @param name
	 */
	public void setPlayer2NumPieces(String numPieces) {
		player2NumPieces.setText(numPieces);
		repaint();
	}
	
	/**
	 * Updates the Icon displayed for player 1
	 * @param img
	 */
	public void setPlayer1Icon(BufferedImage img) {
		BufferedImage bi = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		g.drawImage(img, 0, 0, 32, 32, null);
		g.dispose();
		ImageIcon playerIcon = new ImageIcon(bi);
		player1Label.setIcon(playerIcon);
		repaint();
	}

	/**
	 * Updates the Icon displayed for player 2
	 * @param img
	 */
	public void setPlayer2Icon(BufferedImage img) {
		BufferedImage bi = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		g.drawImage(img, 0, 0, 32, 32, null);
		g.dispose();
		ImageIcon playerIcon = new ImageIcon(bi);
		player2Label.setIcon(playerIcon);
		repaint();
	}

	public JButton getPlayButton() {
		return playButton;
	}

	public JCheckBox getContinuousModeCheckBox() {
		return continuousModeCheckBox;
	}
	
	/**
	 * This function gives us the ability to add mouse listeners outside the GUI class
	 * @param listener
	 * @param listener2
	 */
	public void addBoardListeners(MouseListener listener, MouseMotionListener listener2) {
		othelloView.addMouseListener(listener);
		othelloView.addMouseMotionListener(listener2);
	}
	
	/**
	 * This function gives us the ability to add mouse listeners outside the GUI class
	 * @param listener
	 * @param listener2
	 */
	public void addGUIListeners(ActionListener listener) {
		continuousModeCheckBox.addActionListener(listener);
		pauseAfterGameCheckBox.addActionListener(listener);
		pauseOnWarningCheckBox.addActionListener(listener);
		speedComboBox.addActionListener(listener);
		playButton.addActionListener(listener);
		// Menu Items
		humanMatchMenuItem.addActionListener(listener);
		tournmentMenuItem.addActionListener(listener);
		exitMenuItem.addActionListener(listener);
		resetStatsMenuItem.addActionListener(listener);
		tournamentRoundRobinsMenuItem.addActionListener(listener);
	}
}
