package othello;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Player {

	protected String name;
	protected String iconFile;

	private Color color;
	private int id;
	protected BufferedImage img;
	private int myCaptures;
	private int opponentCaptures;
	private int opponentID;
	private boolean isFirstPlayer;

	public Player(String iconFileName, String n, int id) {
		try {
			img = ImageIO.read(new File("images" + System.getProperty("file.separator") + iconFileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.name = n;
		this.id = id;
		myCaptures = 0;
		opponentCaptures = 0;
	}

	public Player(Color c, String n, int id) {
		color = c;
		name = n;
		this.id = id;
		img = new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB);
		Graphics g = img.getGraphics();
		g.setColor(color);
		g.fillOval(0, 0, 64, 64);
		myCaptures = 0;
		opponentCaptures = 0;
	}
	
	/** Used by AIPlayers to return a place to move on the board
	 * @return Location to play on current turn
	 */
	public Location chooseMove(int[][] idArray) {
		return null;
	}
	
	public String getName() {
		return name;
	}

	public Color getColor() {
		return color;
	}
	
	public int getID() {
		return id;
	}
	
	public void setOpponentID(int oppID) {
		opponentID = oppID;
	}
	
	public int getOpponentID() {
		return opponentID;
	}
	
	public void setIsFirstPlayer(boolean isFirst) {
		isFirstPlayer = isFirst;
	}
	
	public boolean getIsFirstPlayer() {
		return isFirstPlayer;
	}
	
	public BufferedImage getImage() {
		return img;
	}

	/**
	 * Getter for number of captures you have made.
	 * @return Total number of pieces you have captured.
	 */
	public int myCaptures() {
		return myCaptures;
	}

	/**
	 * Getter for number of captures your opponent has made.
	 * @return Total number of pieces your opponent has captured.
	 */
	public int opponentCaptures() {
		return opponentCaptures;
	}

	/**
	 * Setter for player name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Setter for number of captures you have made.
	 */
	public void setMyCaptures(int myCaptures) {
		this.myCaptures = myCaptures;
	}

	/**
	 * Setter for number of captures your opponent has made.
	 */
	public void setOpponentCaptures(int opponentCaptures) {
		this.opponentCaptures = opponentCaptures;
	}
}