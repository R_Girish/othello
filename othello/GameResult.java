package othello;

public class GameResult implements Comparable<GameResult> {

	private Player player;
	private double points;

	public GameResult(Player player, double points) {
		this.player = player;
		this.points = points;
	}
	
	public Player getPlayer() {
		return player;
	}

	public double getPoints() {
		return points;
	}

	public void addPoints(double points) {
		this.points += points;
	}

	// Compare by total points
	public int compareTo(GameResult other) {
		return (int)(10*points - 10*other.getPoints());
	}
	
	public String toString() {
		return "Name: " + player.getName() + " Points: " + points;
	}
}
