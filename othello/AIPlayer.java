package othello;

public abstract class AIPlayer extends Player {

	// When turned on, printDebugMessage method prints to standard output.  Otherwise, output is suppressed.
	public static boolean debugMode = false;
	
	public AIPlayer(String iconFile, String n, int id) {
		super(iconFile, n, id);
	}

	/**
	 * Instead of printing messages to the console using System.out.printl(), use this
	 * method.  That way, printing can be turned on/off without your having to comment
	 * out your debug printing lines
	 * 
	 * @param str The String to write to the console when DEBUG_MODE is turned on
	 */
	public void printDebugMessage(String str) {
		if (debugMode) System.out.println(str);
	}
	
	/**
	 * Override this method and put your game logic here.  To look at the board,
	 * you can access the 'board' variable inherited from Player.
	 * 
	 * @return The Location your player will place a piece next
	 */
	public abstract Location chooseMove(int[][] idArray);

}
