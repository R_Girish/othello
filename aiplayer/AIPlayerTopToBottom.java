/**
 * 
 */
package aiplayer;

import java.util.ArrayList;

import othello.AIHelper;
import othello.AIPlayer;
import othello.Location;

/**
 * @author Eric Ferrante
 *
 * This AI Player always moves in the topmost row available and as far left within that row
 */
public class AIPlayerTopToBottom extends AIPlayer {

	private static String name = "Basic AI - Top To Bottom";
	private static String iconFile = "yellow.png";
	
	public AIPlayerTopToBottom(int id) {
		super(iconFile, name, id);
	}

	@Override
	public Location chooseMove(int[][] idArray) {
		ArrayList<Location> validMoves = AIHelper.getValidMoves(idArray, this.getID(), this.getOpponentID());

		Location minLoc = null;
		int minRow = Integer.MAX_VALUE;

		for(Location loc : validMoves) {
			if (loc.getRow() < minRow) {
				minLoc = loc;
				minRow = loc.getRow();
			}
		}

		return minLoc;	
	}	
}