package aiplayer;

import java.util.ArrayList;

import othello.AIHelper;
import othello.AIPlayer;
import othello.Location;

/**
 * <b>StudentRelevant</b>
 * <p/>
 * This is a basic AI player that always picks random (valid) moves.  The AI(s) that you design should <i>hopefully</i>
 * be able to beat this one.
 */
public class AIPlayerBasicRandom extends AIPlayer {

	private static String name = "BASIC AI - Random";
	private static String iconFile = "white.png";
	
	public AIPlayerBasicRandom(int id) {
		super(iconFile, name, id);
	}

	@Override
	public Location chooseMove(int[][] idArray) {
		// determine which moves are available/valid
		ArrayList<Location> validMoves = AIHelper.getValidMoves(idArray, this.getID(), this.getOpponentID());
		
		if (validMoves.size() > 0)
		{
			// pick random available move
			return validMoves.get((int)(Math.random()*validMoves.size()));
		}
		else
		{
			// no moves available
			return null;	
		}
	}
	
}