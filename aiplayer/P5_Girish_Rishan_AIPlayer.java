package aiplayer;

import java.util.ArrayList;

import org.omg.CosNaming.IstringHelper;

import othello.AIHelper;
import othello.AIPlayer;
import othello.Board;
import othello.Location;
import othello.OthelloLogic;

/**
 * TODO: Copy and rename this file (and class name) to something containing the
 * substring "AIPlayer" and your LastnameFirstname plus any other
 * flavor/descriptive words you'd like. Note that when we share your v1 AI with
 * the rest of the class, they won't be able to see your source code, but they
 * will see your class's name (i.e. we'll be sharing the compiled ".class"
 * files), so don't reveal your secret strategy in the class name.
 * <p/>
 * 
 * <b>Tip:</b> You don't need to modify any other code in the rest of this
 * project, but you can search for the keyword <b>StudentRelevant</b> to find
 * helper methods you can call (specifically the methods in {@link AIHelper} or
 * spots you might consider modifying to help test/debug your code (e.g.
 * {@link OthelloController#initializeBoardPieces}
 */
public class P5_Girish_Rishan_AIPlayer extends AIPlayer {

	private static String name = "TODO: Put your name here"; // display name
	// Note that when we share your version 1 AI with the rest of the class,
	// everyone will be able to see your AI's name (in addition
	// to class name, as mentioned above). So you may want to avoid giving away
	// your secret strategy in the name.
	// e.g. pick something non-revealing like "Shimshock_v1" or
	// "Ferrante Magic Sauce" (the same thinking applies to your

	private static String iconFile = "gnomeChild.jpg";

	// TODO: make or find a icon to use as your tile image (during tournament
	// play)
	// and place the file in the "images" folder.

	OthelloLogic logic;
	Location bestMove;

	public P5_Girish_Rishan_AIPlayer(int id) {
		super(iconFile, name, id);
		debugMode = false; // printDebugMessage() method prints only when
							// debugMode is set to true (See AIPlayer.java)

		// if you want to do any setup stuff for your AI, you can do it here (so
		// you don't have to do it during every move)
	}

	/**
	 * TODO: This is the method you need to write for this project ... it's the
	 * brains of your AI.
	 * 
	 */
	@Override
	public Location chooseMove(int[][] idArray) {
		// determine which moves are available/valid
		ArrayList<Location> validMoves = AIHelper.getValidMoves(idArray,
				this.getID(), this.getOpponentID());

		if (validMoves.size() == 0) {
			// no moves available (note: the game framework might just
			// automatically skip your turn and not call this method if no moves
			// available)
			return null;
		} else // some available moves to analyze
		{
	
			for (Location move: validMoves) {
				if(isCorner(move, idArray)) return move;
			}
			int score = search(idArray, getID(), 0, 0, 4);
			return bestMove;
		}
	}

	private boolean isCorner(Location move, int[][] idArray) {
		// TODO Auto-generated method stub
		int row = move.getRow();
		int col = move.getCol();
		if((row == 0 && col == 0) || (row == 0 && col == idArray[0].length - 1) || (row == idArray.length - 1 && col == idArray[0].length - 1) || (row == idArray.length - 1 && col == 0)) return true;
		
		return false;
	}
	
	
	public int search(int[][] board, int playerID, int alpha, int beta, int depth) {
		int record = Integer.MIN_VALUE;
		Location maxMove = null;
		int[][] subBoard = board.clone();
		if (depth <= 0 || boardIsFull(board)) {
			record = (playerID == this.getID())? countMyScore(board) : countOpponentScore(board);
		} else {
			ArrayList<Location> possibleMoves;
			if(playerID == this.getID()) {
				possibleMoves = AIHelper.getValidMoves(board, playerID, this.getOpponentID());
			}else{
				possibleMoves = AIHelper.getValidMoves(board, this.getOpponentID(), this.getID());
			}
			
			if (possibleMoves.size() != 0) {
				for (Location nextPossibleMove : possibleMoves) {
					subBoard = board.clone();
					int result;
					if(playerID == this.getID()) {
						subBoard = AIHelper.tryMove(subBoard, nextPossibleMove, playerID, this.getOpponentID());
						result = -search(subBoard, this.getOpponentID(), alpha, beta, depth - 1); 
					}else{
						subBoard = AIHelper.tryMove(subBoard, nextPossibleMove, this.getOpponentID(), this.getID());
						result = -search(subBoard, this.getID(), alpha, beta, depth - 1); 
					}
					
					if (result > record) {
						record = result;
						maxMove = nextPossibleMove;
					}
				}
			} else {
				record = -search(subBoard, playerID, alpha, beta, depth - 1);
			}
		}
		bestMove = maxMove;
		return record;
	}

	
	private int countOpponentScore(int[][] tryMove) {
		// TODO Auto-generated method stub
		int id = this.getOpponentID();
		int count = 0;
		// TODO Auto-generated method stub
		for (int i = 0; i < tryMove.length; i++) {
			for (int j = 0; j < tryMove[0].length; j++) {
				if (tryMove[i][j] == id)
					count++;
			}
		}
		return count;
	}

	private int countMyScore(int[][] tryMove) {
		// TODO Auto-generated method stub
		int id = this.getID();
		int count = 0;
		// TODO Auto-generated method stub
		for (int i = 0; i < tryMove.length; i++) {
			for (int j = 0; j < tryMove[0].length; j++) {
				if (tryMove[i][j] == id)
					count++;
			}
		}
		return count;
	}

	private boolean boardIsFull(int[][] arr) {
		logic = new OthelloLogic(AIHelper.boardFromArray(arr));
		return logic.boardIsFull();

	}

}
